package lottery;

/**
 * Created by Anh on 1/22/2016.
 */
public class InvalidLotteryDate extends Exception {
    public InvalidLotteryDate(String message) {
        super(message);
    }
}
