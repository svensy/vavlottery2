package lottery;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.*;

/**
 * Created by Anh on 1/17/2016.
 */
abstract class LotteryInfo {
    abstract public LotteryResult getResult(String area, Date... date);
}
